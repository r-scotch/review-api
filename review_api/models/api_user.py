from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm import relationship
from passlib.hash import sha256_crypt
from datetime import datetime
import logging

from review_api.models import Status, Role
from review_api.models import Base, Session
from review_api.config import config

logger = logging.getLogger('ApiUser')

class ApiUser(Base):
    __tablename__ = 'api_users'
    id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False, unique=True)
    display_name = Column(String, nullable=False)
    password = Column(String, nullable=False)
    role = Column(String, nullable=False)
    status = Column(String, nullable=False, default=Status.pending.name)
    created_at = Column(String, nullable=False)
    last_request = Column(String, nullable=True)

    @staticmethod
    def update_password(username, new_pass):
        username = username.lower()
        password = sha256_crypt.encrypt(new_pass)

        session = Session()
        session.query(ApiUser)\
            .filter(ApiUser.username == username)\
            .update({'password': password})
        session.commit()

    @staticmethod
    def update_last_request(username, timestamp=None):
        username = username.lower()
        if not timestamp:
            timestamp = datetime.utcnow()
        last_request = timestamp.strftime('%Y-%m-%d %H:%M')

        session = Session()
        session.query(ApiUser)\
            .filter(ApiUser.username == username)\
            .update({'last_request': last_request})
        session.commit()

    @staticmethod
    def get(limit=100, offset=0):
        session = Session()
        return session.query(ApiUser)\
            .order_by(ApiUser.display_name)\
            .offset(offset)\
            .limit(limit)\
            .all()

    @staticmethod
    def newest(limit=100, offset=0):
        session = Session()
        return session.query(ApiUser)\
            .order_by(ApiUser.created_at.desc(), ApiUser.display_name)\
            .offset(offset)\
            .limit(limit)\
            .all()

    @staticmethod
    def find(username):
        session = Session()
        username = username.lower()
        try:
            user = session.query(ApiUser)\
                .filter(ApiUser.username == username)\
                .one_or_none()
        except MultipleResultsFound as e:
            logger.exception('Multiple ApiUsers with name ' + username)
            return None
        else:
            return user


    @staticmethod
    def register(username, password, role=Role.user, status=Status.pending):
        session = Session()
        hpass = sha256_crypt.encrypt(password)
        user = ApiUser(
            username=username.lower(),
            display_name=username,
            password=hpass,
            role=role.name,
            status=status.name,
            created_at=datetime.now().strftime('%Y-%m-%d')
        )
        session.add(user)
        try:
            session.commit()
        except Exception as e:
            logger.exception(
                "Cannot register new user<{}>".format(username),
                e)
            return False
        else:
            return True
