from enum import Enum

Status = Enum('Status', ['pending', 'approved', 'denied', 'revoked'])