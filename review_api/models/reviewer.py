from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from review_api.models import Base, Session

class Reviewer(Base):
    __tablename__ = 'reviewers'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    display_name = Column(String, nullable=False)
    reviews = relationship("Review", back_populates="reviewer", cascade="save-update, delete", lazy='dynamic')
    bottles = relationship("Bottle", secondary="reviews", back_populates="reviewers", lazy='dynamic')

    @staticmethod
    def find(reviewer_name):
        reviewer_name = reviewer_name.lower()
        session = Session()
        return session.query(Reviewer)\
            .filter(Reviewer.name == reviewer_name)\
            .first()

    @staticmethod
    def get(limit=100, offset=0):
        session = Session()
        return session.query(Reviewer)\
            .offset(offset)\
            .limit(limit)\
            .all()
