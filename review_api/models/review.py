from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from review_api.models import Base, Session

class Review(Base):
    __tablename__ = 'reviews'
    id = Column(String, primary_key=True)
    bottle_id = Column(Integer, ForeignKey('bottles.id'))
    reviewer_id = Column(Integer, ForeignKey('reviewers.id'))
    bottle = relationship("Bottle", back_populates="reviews")
    reviewer = relationship("Reviewer", back_populates="reviews")

    link = Column(String, nullable=False)
    rating = Column(Integer, nullable=True)
    price = Column(Integer, nullable=True)
    date = Column(Integer, nullable=False)
    title = Column(String, nullable=True)
    subreddit = Column(String, nullable=False)

    __mapper_args__ = {
        "order_by": [date.desc(), bottle_id]
    }

    @staticmethod
    def find(review_id):
        session = Session()
        return session.query(Review)\
            .filter(Review.id == review_id)\
            .first()

    @staticmethod
    def latest(limit=100, offset=0):
        session = Session()
        return session.query(Review)\
            .order_by(Review.date.desc())\
            .offset(offset)\
            .limit(limit)\
            .all()

    @staticmethod
    def by_subreddit(subreddit, limit=100, offset=0):
        session = Session
        return session.query(Review)\
            .filter(Review.subreddit == subreddit.lower())\
            .order_by(Review.date.desc())\
            .offset(offset)\
            .limit(limit)\
            .all()
