from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from review_api.models import Base, Session

class Bottle(Base):
    __tablename__ = 'bottles'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    region = Column(String, nullable=False)
    reviews = relationship("Review", back_populates="bottle", cascade="save-update, delete", lazy='dynamic')
    reviewers = relationship("Reviewer", secondary="reviews", back_populates="bottles", lazy='dynamic')

    @staticmethod
    def find(bottle_id):
        session = Session()
        return session.query(Bottle)\
            .filter(Bottle.id == bottle_id)\
            .first()

    @staticmethod
    def get(limit=100, offset=0):
        session = Session()
        return session.query(Bottle.id, Bottle.name, Bottle.region)\
            .order_by(Bottle.name)\
            .offset(offset)\
            .limit(limit)\
            .all()

