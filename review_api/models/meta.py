from sqlalchemy import Column, ForeignKey, Integer, String
from review_api.models import Base, Session
from datetime import datetime

class Meta(Base):
    __tablename__ = 'metadata'
    key = Column(String, nullable=False, primary_key=True)
    value = Column(String, nullable=False)
    updated_at = Column(Integer, nullable=False)

    @staticmethod
    def update(key, value):
        time = datetime.utcnow().timestamp()
        session = Session()
        session.query(Meta)\
            .filter(Meta.key == key)\
            .update({'value': value, 'updated_at': int(time)})
        session.commit()

    @staticmethod
    def find(key):
        session = Session()
        return session.query(Meta)\
            .filter(Meta.key == key)\
            .first()

    @staticmethod
    def get():
        session = Session()
        return session.query(Meta)\
            .order_by(Meta.key)\
            .all()
