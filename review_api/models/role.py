from enum import Enum

Role = Enum('Role', ['user', 'admin'])