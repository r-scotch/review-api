import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from review_api.config import config

from review_api.models.role import Role
from review_api.models.status import Status

engine = sqlalchemy.create_engine(config.SQLALCHEMY_DATABASE_URI)
Base = declarative_base()
factory = sessionmaker(bind=engine)
# factory.configure(bind=engine)
Session = scoped_session(factory)

from review_api.models.api_user import ApiUser
from review_api.models.meta import Meta
from review_api.models.bottle import Bottle
from review_api.models.reviewer import Reviewer
from review_api.models.review import Review

