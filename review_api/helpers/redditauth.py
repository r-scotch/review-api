import urllib.parse
import requests
from review_api.config import config
import uuid

# Authorization process as layed out in https://gist.github.com/kemitche/9749639
# https://github.com/reddit/reddit/wiki/OAuth2
def make_auth_url(session):
    '''Create an authorization url for permanent identity access'''
    state = str(uuid.uuid4())
    session[state] = True
    params = {
        'client_id': config.REDDIT_CLIENT_ID,
        'response_type': 'code',
        'state': state,
        'redirect_uri': config.BASE_URL + config.REDDIT_REDIRECT_PATH,
        'duration': 'temporary',
        'scope': 'identity'
    }
    url = config.REDDIT_AUTH_URL + '?' + urllib.parse.urlencode(params)
    return url

def get_token(code):
    client_auth = requests.auth.HTTPBasicAuth(config.REDDIT_CLIENT_ID, config.REDDIT_CLIENT_SECRET)
    post_data = {
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': config.BASE_URL + config.REDDIT_REDIRECT_PATH
    }
    headers = {'User-Agent': config.REDDIT_USER_AGENT}
    response = requests.post(
        config.REDDIT_ACCESS_URL,
        auth = client_auth,
        headers = headers,
        data = post_data
    )
    token_json = response.json()
    return token_json['access_token'] 

def get_username(access_token):
    headers = {'User-Agent': config.REDDIT_USER_AGENT}
    headers.update({ 'Authorization': 'bearer ' + access_token })
    response = requests.get('https://oauth.reddit.com/api/v1/me', headers=headers)
    me_json = response.json()
    try:
        return me_json['name']
    except KeyError:
        None

def is_authorised(session):
    if 'username' in session:
        return True
    if not 'access_token' in session:
        return False
    username = get_username(session['access_token'])
    if not username:
        session.pop('access_token')
        return False
    session['username'] = username
    return True