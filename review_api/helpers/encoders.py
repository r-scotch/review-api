class ReviewEncoder:
    def encode(self, review):
        if type(review) == list:
            return [self.encode(rev) for rev in review]
        return {
            'id': review.id,
            'reviewer_id': review.reviewer_id,
            'reviewer': review.reviewer.name,
            'bottle_id': review.bottle_id,
            'bottle': review.bottle.name,
            'link': review.link,
            'rating': review.rating,
            'price': review.price,
            'date': review.date,
            'title': review.title,
            'subreddit': review.subreddit
        }

class ReviewerEncoder:
    def encode(self, reviewer):
        if type(reviewer) == list:
            return [self.encode(rev) for rev in reviewer]
        return {
            'id': reviewer.id,
            'name': reviewer.name,
            'display_name': reviewer.display_name
        }

class BottleEncoder:
    def encode(self, bottle):
        if type(bottle) == list:
            return [self.encode(b) for b in bottle]
        return {
            'id': bottle.id,
            'name': bottle.name,
            'region': bottle.region
        }