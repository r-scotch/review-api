import logging, time
from flask import request, abort, current_app
from jose import jwt
from jose.exceptions import ExpiredSignatureError, JWSError
from passlib.hash import sha256_crypt
from sqlalchemy.exc import OperationalError

from review_api.config import config
from review_api.models import ApiUser

logger = logging.getLogger('Security')

def authenticate():
    credentials = request.authorization
    if not credentials:
        return None
    user = ApiUser.find(credentials.username.lower())
    if not user:
        return None
    if sha256_crypt.verify(credentials.password, user.password):
        try:
            ApiUser.update_last_request(credentials.username)
        except OperationalError:
            pass 
        return user

def decode_token():
    prefix = 'Bearer '
    try:
        token = request.headers['Authorization']
    except KeyError:
        abort(401)
    token = token[len(prefix):]
    try:
        payload = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])
    # invalid signature
    except JWSError as e:
        logger.exception('Invalid token.', e)
    else:
        return payload

def authorized(roles):
    data = decode_token() 
    if not data:
        return False
    return data and data['role'] in roles

def authorize(roles):
    def wrapper(func):
        def wrapped_func(*args, **kwargs):
            if not config.DEBUG and not authorized(roles):
                abort(403)
            return func(*args, **kwargs)
        return wrapped_func
    return wrapper

