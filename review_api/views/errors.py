from flask import make_response, jsonify, render_template
from review_api import app
from jose.exceptions import ExpiredSignatureError
from werkzeug.exceptions import HTTPException
from sqlalchemy.exc import OperationalError

@app.errorhandler(ExpiredSignatureError)
def token_expired(error):
    return make_response(jsonify({'reason': "expired token"}), 401)

@app.errorhandler(HTTPException)
def http_error(error):
    return render_template('error.html', status=error.code, message=error.description, user=None), error.code

@app.errorhandler(OperationalError)
def sql_error(error):
    msg = 'The database is currently being updated. This means you cannot register or change your password at the moment. Please try again later.'
    return render_template('error.html', status=423, message=msg, user=None), 423
    
