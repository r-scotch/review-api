from flask import Blueprint, abort, session, render_template, request, redirect, url_for

from review_api.helpers import security, redditauth
from review_api.models import ApiUser, Meta, Status, Role, Session
from review_api.config import config

blueprint = Blueprint('portal', __name__, template_folder='templates')

@blueprint.route('/')
def home():
    user, username, auth_url = None, None, None
    if redditauth.is_authorised(session):
        username = session['username']
        user = ApiUser.find(session['username'])
    else:
        auth_url = redditauth.make_auth_url(session)
    return render_template('portal/index.html', base_url=config.BASE_URL, auth_url=auth_url, user=user, username=username)

@blueprint.route('/changelog')
def changelog():
    user, username, auth_url = None, None, None
    if redditauth.is_authorised(session):
        username = session['username']
        user = ApiUser.find(session['username'])
    else:
        auth_url = redditauth.make_auth_url(session)
    return render_template('portal/changelog.html', base_url=config.BASE_URL, auth_url=auth_url, user=user, username=username)

@blueprint.route('/user')
def user():
    if not redditauth.is_authorised(session):
        return redirect(url_for('portal.home'))
    username = session['username']
    user = ApiUser.find(username)
    if not user:
        return render_template('portal/register.html', username=username, user=user)
    return render_template('portal/user.html', user=user)

@blueprint.route('/user/register', methods=['POST'])
def registration():
    if not redditauth.is_authorised(session):
        return abort(401)
    username = session['username']
    password = request.form['password']
    if ApiUser.register(username, password):
        return redirect(url_for('portal.user'))
    else:
        abort(500)

@blueprint.route('/user/change_password', methods=['POST'])
def change_password():
    if not redditauth.is_authorised(session):
        return abort(401)
    username = session['username']
    password = request.form['password']
    ApiUser.update_password(username, password)
    return redirect(url_for('portal.user'))

@blueprint.route('/admin/')
def admin():
    if not redditauth.is_authorised(session):
        abort(401)

    user = ApiUser.find(session['username'])
    if user.role != Role.admin.name:
        abort(403)

    limit = 10
    users = ApiUser.newest(limit=limit)
    statuss = [status.name for status in Status]
    api_version = Meta.find('api_version')
    meta = {'api': 1}
    if api_version:
        meta['api'] = api_version.value
    return render_template('portal/admin.html', user=user, users=users, statuss=statuss, meta=meta)

@blueprint.route('/admin/users/', defaults={'page': 1})
@blueprint.route('/admin/users/<int:page>')
def admin_users(page):
    if not redditauth.is_authorised(session):
        abort(401)

    user = ApiUser.find(session['username'])
    if user.role != Role.admin.name:
        abort(403)

    limit = 10
    offset = (page - 1) * limit
    users = ApiUser.newest(limit=limit, offset=offset)
    statuss = [status.name for status in Status]
    return render_template('portal/admin_users.html', user=user, users=users, statuss=statuss)

@blueprint.route('/admin/users/<name>')
def admin_user(name):
    if not redditauth.is_authorised(session):
        abort(401)

    user = ApiUser.find(session['username'])
    if user.role != Role.admin.name:
        abort(403)

    api_user = ApiUser.find(username=name.lower())
    if not api_user:
        abort(404)

    statuss = [status.name for status in Status]
    return render_template('portal/admin_user.html', user=user, api_user=api_user, statuss=statuss)

@blueprint.route('/admin/process_registration', methods=['POST'])
def process_registration():
    if not redditauth.is_authorised(session):
        abort(401)

    user = ApiUser.find(session['username'])
    if user.role != Role.admin.name:
        abort(403)

    usernames = request.form.getlist('usernames[]')
    approvals = request.form.getlist('approvals[]')
    db_session = Session()
    for index, username in enumerate(usernames):
        db_session.query(ApiUser)\
            .filter(ApiUser.username == username)\
            .update({'status': approvals[index]})

    db_session.commit()
    return redirect(url_for('portal.admin'))

@blueprint.route('/admin/update_meta', methods=['POST'])
def update_meta():
    if not redditauth.is_authorised(session):
        abort(401)

    user = ApiUser.find(session['username'])
    if user.role != Role.admin.name:
        abort(403)
    
    Meta.update(request.form['key'], request.form['value'])
    return redirect(request.referrer)

@blueprint.route('/reddit')
def reddit_callback():
    error = request.args.get('error', '')
    if error:
        return "Error: " + error
    state = request.args.get('state', '')
    if not state in session:
        abort(400)
    session.pop(state)
    code = request.args.get('code')
    session['access_token'] = redditauth.get_token(code)
    return redirect(url_for('portal.user'))    

@blueprint.route('/logout')
def logout():
    try:
        session.pop('username')
        session.pop('access_token')
    except KeyError:
        pass
    return redirect(url_for('portal.home'))
