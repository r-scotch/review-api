from datetime import datetime, timedelta
from jose import jwt
from flask import Blueprint, abort, jsonify, current_app
from review_api.config import config

from flask_restful import Resource, Api

from review_api.helpers import security
from review_api.models import ApiUser, Status

blueprint = Blueprint('auth', __name__)
auth = Api(blueprint)

class TokenResource(Resource):
    def get(self):
        user = security.authenticate()
        if not user:
            abort(401)
        if user.status != Status.approved.name:
            abort(403)
        exp_utc = datetime.utcnow() + timedelta(seconds=config.TOKEN_EXP)
        exp_server = datetime.now() + timedelta(seconds=config.TOKEN_EXP)
        exp_string = exp_utc.strftime('%a, %d %b %Y %H:%M:%S') + ' UTC'
        payload = {
            'sub': str(user.id),
            'role': user.role,
            'exp': int(exp_server.timestamp())
        }
        token = jwt.encode(payload, config.SECRET_KEY, algorithm=config.JWT_ALG)
        return {'token': token}, 200, {'Expires': exp_string}

auth.add_resource(TokenResource, '/token')
