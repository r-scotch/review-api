from flask import Blueprint, abort, jsonify, request
from flask_restful import Resource, Api, reqparse
from flask_restful import reqparse
import time

from review_api.models import Meta as MetaData
from review_api.models import Bottle, Review, Reviewer
from review_api.config import config
from review_api.helpers.security import authorize
from review_api.helpers.encoders import ReviewEncoder, ReviewerEncoder, BottleEncoder

blueprint = Blueprint('api', __name__)

parser = reqparse.RequestParser()
parser.add_argument('limit', type=int, default=config.RESULT_LIMIT, help='Maximum number of records to retrieve.')
parser.add_argument('offset', type=int, default=0, help='Offset of which to retrieve records from.')

api = Api(blueprint)

def parse_args():
    args = parser.parse_args()
    args['limit'] = max(1, min(config.RESULT_LIMIT, args['limit'])) # 1 <= limit <= 51
    args['offset'] = max(0, args['offset']) # 0 <= offset
    return args

class BottleCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self):
        args = parse_args()
        bottles = Bottle.get(limit=args['limit'], offset=args['offset'])
        return BottleEncoder().encode(bottles)

class BottleResource(Resource):
    @authorize(['user', 'admin'])
    def get(self, id):
        args = parse_args()
        bottle = Bottle.find(id)
        return BottleEncoder().encode(bottle)

class ReviewCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self):
        args = parse_args()
        reviews = Review.latest(limit=args['limit'], offset=args['offset'])
        return ReviewEncoder().encode(reviews)

class ReviewResource(Resource):
    @authorize(['user', 'admin'])
    def get(self, id):
        args = parse_args()
        review = Review.find(id)
        return ReviewEncoder().encode(review)

class ReviewsBySubredditCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self, subreddit):
        args = parse_args()
        reviews = Review.by_subreddit(subreddit, limit=args['limit'], offset=args['offset'])
        return ReviewEncoder().encode(reviews)

class ReviewerCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self):
        args = parse_args()
        reviewers = Reviewer.get(limit=args['limit'], offset=args['offset'])
        return ReviewerEncoder().encode(reviewers)

class ReviewerResource(Resource):
    @authorize(['user', 'admin'])
    def get(self, name):
        args = parse_args()
        reviewer = Reviewer.find(name)
        return ReviewerEncoder().encode(reviewer)

class BottleReviewsCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self, id):
        args = parse_args()
        bottle = Bottle.find(id)
        if not bottle:
            abort(404)
        return ReviewEncoder().encode(bottle.reviews\
            .limit(args['limit'])\
            .offset(args['offset'])\
            .all()
        )

class BottleReviewersCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self, id):
        args = parse_args()
        bottle = Bottle.find(id)
        if not bottle:
            abort(404)
        return ReviewerEncoder().encode(bottle.reviewers\
            .limit(args['limit'])\
            .offset(args['offset'])\
            .all()
        )

class ReviewerReviewsCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self, name):
        args = parse_args()
        reviewer = Reviewer.find(name)
        if not reviewer:
            abort(404)
        return ReviewEncoder().encode(reviewer.reviews\
            .limit(args['limit'])\
            .offset(args['offset'])\
            .all()
        )

class ReviewerReviewsBySubredditCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self, name, subreddit):
        args = parse_args()
        reviewer = Reviewer.find(name)
        if not reviewer:
            abort(404)
        return ReviewEncoder().encode(
            reviewer.reviews\
                .filter(Review.subreddit == subreddit.lower())\
                .limit(args['limit'])\
                .offset(args['offset'])\
                .all()
        )

class ReviewerBottlesCollection(Resource):
    @authorize(['user', 'admin'])
    def get(self, name):
        args = parse_args()
        reviewer = Reviewer.find(name)
        if not reviewer:
            abort(404)
        return BottleEncoder().encode(reviewer.bottles\
            .limit(args['limit'])\
            .offset(args['offset'])\
            .all()
        )

class Meta(Resource):
    def get(self):
        # todo insert, update and retrieve from database
        api_meta = MetaData.find('api_version')
        db_meta = MetaData.find('database_updated')
        return jsonify({
            'database_update': db_meta.updated_at,
            'api_update': api_meta.updated_at,
            'api_version': int(api_meta.value)
        })

api.add_resource(BottleCollection, '/bottles', '/bottles/')
api.add_resource(BottleResource, '/bottles/<int:id>')

api.add_resource(ReviewCollection, '/reviews', '/reviews/')
api.add_resource(ReviewResource, '/reviews/<int:id>')
api.add_resource(ReviewsBySubredditCollection, '/reviews/by_subreddit/<subreddit>')

api.add_resource(ReviewerCollection, '/reviewers', '/reviewers/')
api.add_resource(ReviewerResource, '/reviewers/<name>')

api.add_resource(BottleReviewsCollection, '/bottles/<int:id>/reviews/')
api.add_resource(BottleReviewersCollection, '/bottles/<int:id>/reviewers/')
api.add_resource(ReviewerReviewsCollection, '/reviewers/<name>/reviews/')
api.add_resource(ReviewerReviewsBySubredditCollection, '/reviewers/<name>/reviews/by_subreddit/<subreddit>')
api.add_resource(ReviewerBottlesCollection, '/reviewers/<name>/bottles/')

api.add_resource(Meta, '/meta')
