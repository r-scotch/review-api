{% extends 'layout.html' %}

{% block subtitle %} Documentation {% endblock %}
{% block content %}
<div class="section row">
    <div class="large-12 columns">
        <p>
            The Review API was originally created to decouple the data and the logic from <a href="https://reddit.com/u/review_bot" target="_blank">ReviewBot</a>. Since we were creating an API, we figured others might want to make use of it as well.
            The rest of this document explains how to get access and what data is available.
        </p>

        <p>In a nutshell, this API offers all information pertaining to reviews posted on the subreddits <a href="https://reddit.com/r/scotch" target="_blank">/r/Scotch</a>, <a href="https://reddit.com/r/bourbon" target="_blank">/r/Bourbon</a> and <a href="https://reddit.com/r/worldwhisky" target="_blank">/r/WorldWhisky</a>. Once every few weeks the database is updated using their official archive.
        </p>

        <h3 id="api-access">API Access <a href="#api-access" class="anchor">&#182;</a></h3>
        <p>
            The server uses HTTP Basic Authentication to authenticate a user for the first time. If the authentication is successful, a <a href="https://jwt.io" target="_blank" title="JSON Web Token">JWT</a> is sent back. Subsequent requests should then have this token attached to the HTTP Authorization header. The server validates the token, if it has expired it returns with status 401 and a JSON response (shown below). If the token is valid, the requested data is returned.
        </p>
        <pre>{
    "reason": "expired token"
}</pre>

        <h3 id="create-account">Creating an account <a href="#create-account" class="anchor">&#182;</a></h3>
        <p>
            In order to create an account, you first need to login via Reddit. Since this API contains only data specific to a few subreddits, we believe it should not matter for you to need to login via Reddit. This step makes it easier for us to keep track of who is using the API and to contact anyone in case we need to.
        </p>
        <p>
            Once logged in, you are redirected to the registration page. Your username will always match your Reddit username, but you should use a different password for the API. In case you clicked this page away, no worries. Whenever you are logged in and have not yet registered an API account, you can click on <a title="Review API: Create account" href="{{ url_for('portal.user') }}">Create account</a> in the top right corner to proceed registration.
        </p>
        <p>
            Once you have registered, you will notice that your account status is set to <span class="inline_code">pending</span>. For the time being, we want to manually approve new registrations as our hosting is very limited. We will either send you a message on Reddit or you can check back within a week to see if your account has been approved. Once it has been approved, you can start using the API using the process described in the section below.
        </p>
        <p>
            Note: in case you ever forget your API password, just login to this portal again and you can reset it immediately!
        </p>

        <h3 id="auth-flow">Authentication flow <a href="#auth-flow" class="anchor">&#182;</a></h3>
        <p>
            Let's assume in this example your username is <span class="inline_code">Bob</span> and your API password is <span class="inline_code">Password2</span>. To demonstrate the connection, I'll use Python. 
        </p>
        <p>
            So first off, we need to retrieve a JWT. We can do this by sending a GET request to <span class="inline_code">/auth/token</span> with Basic authentication in the Authorization header.
        </p>
        <pre>import json, requests
url = '{{ base_url }}'
response = requests.get(url + '/auth/token', auth=requests.auth.HTTPBasicAuth('Bob', 'Password2'))
data = json.loads(response.content.decode('utf-8'))
token = data['token']</pre>
        <p>
            Next we can send requests to the API by adding the token the Authorization header. <br /><b>Note:</b> please make sure to add the string <span class="inline_code">'Bearer '</span> in front of the token.
        </p>
        <pre>latest_reviews = requests.get(url + '/api/reviews/', headers={'Authorization': 'Bearer ' + token})</pre>
        <p>And that's all!</p>
        <h3 id="resources">Resources <a href="#resources" class="anchor">&#182;</a></h3>
        <p>
            Reviews are saved in relation to the bottle and the reviewer. Using the API you can request any of these three resources. All endpoints return data in JSON format.
        </p>

        <h4>Global optional parameters</h4>
        <p>First let me point out that by default you get the maximum number of results per request (500). If you need less results, you can use the GET parameter <span class="inline_code">limit=&lt;number&gt;</span> to request just <i>number</i> results. If you want the next <i>number</i> of results you can use the GET parameter <span class="inline_code">offset=&lt;offset&gt;</span>. The <i>offset</i> parameter defaults to zero, so getting results 501 to 750 would look like this: <span class="inline_code">/api/&lt;resource&gt;/?offset=500&amp;limit=250</span>.</p>

        <h4>Common endpoints</h4>
        <p>
            Each resource collection can be requested via <span class="inline_code">/api/&lt;resource&gt;/</span><span class="small">*</span>, where &lt;resource&gt; is one of <u>bottles</u>, <u>reviews</u> or <u>reviewers</u>. A specific resource can be retrieved by appending the id: <span class="inline_code">/api/&lt;resource&gt;/&lt;id&gt;</span>.
        </p>
        <p class="small">*Note the trailing slash.</p>

        <h4>Bottles</h4>
        <p><span class="inline_code">/api/bottles/&lt;id&gt;</span> is the endpoint to get a specific bottle. When you append <span class="inline_code">/reviews/</span>, you'll get all the reviews about this bottle. If you want to directly get all the reviewers of a certain bottle, you can append <span class="inline_code">/reviewers/</span>.

        <h4>Reviewers</h4>
        <p>Reviewer resources are a bit different, you can use the Reddit username of the reviewer instead of the id to retrieve a specific one. As with the bottles, appending <span class="inline_code">/reviews/</span> will return all the reviews by the specified reviewer. Likewise for the bottles: append <span class="inline_code">/bottles/</span> to retrieve all the bottles a reviewer has reviewed.</p>

        <h4>Reviews</h4>
        <p>Additionally, wherever you can request a collection of reviews, you can append <span class="inline_code">/by_subreddit/&lt;subreddit&gt;</span> to only get reviews from one subreddit.</p>

        <h4>Metadata</h4>
        <p>In case you are interested in when the data has been updated for the last time or if the API has had updates, you can query <span class="inline_code">/api/meta</span>. Whenever the API gets an update, details will be published on the <a href="{{ url_for('portal.changelog') }}">Changelog</a> page. The metadata endpoint is public, so you do not need to add your token to the Authorization header. The JSON returned looks similar to this:</p>
        <pre>{
  "api_update": 1460494200,
  "api_version": 1,
  "database_update": 1460373816
}</pre>

        <h3 id="endpoints">Endpoint overview <a href="#endpoints" class="anchor">&#182;</a></h3>
        <p>Below is an overview of all the available API endpoints.</p>

        <h4>Public</h4>
        <p>The endpoints that do not require any authentication.</p>
        <pre>/api/meta</pre>

         <h4>Authentication required</h4>
         <p>These endpoints require a valid JWT in the Authorization header. How to acquire a JWT is described <a href="{{ url_for('portal.home') }}#auth-flow">above</a>.</p>
         <pre>
/api/reviews/
/api/reviews/by_subreddit/&lt;subreddit&gt;
/api/reviews/&lt;id&gt;

/api/bottles/
/api/bottles/&lt;id&gt;
/api/bottles/&lt;id&gt;/reviews/
/api/bottles/&lt;id&gt;/reviewers/

/api/reviewers/
/api/reviewers/&lt;name&gt;
/api/reviewers/&lt;name&gt;/bottles/
/api/reviewers/&lt;name&gt;/reviews/
/api/reviewers/&lt;name&gt;/reviews/by_subreddit/&lt;subreddit&gt;
</pre>

        <h3 id="closing-comments">Closing comments <a href="#closing-comments" class="anchor">&#182;</a></h3>
        <p>That's all for now. If you have any questions, bug reports or feature requests, feel free to send <a href="https://www.reddit.com/user/FlockOnFire/">/u/FlockOnFire</a> a message on Reddit!</p>

        <h3 id="FAQ">FAQ <a href="#FAQ" class="anchor">&#182;</a></h3>
        <h4>What's up with the weird domain?</h4>
        <p>This is a personal domain, mostly used for small project or just linking ip addresses to a domain. If this project proves to be popular, I'll make sure to register a proper domain name!</p>
        
    </div>
</div>
{% endblock %}
