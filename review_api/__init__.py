from flask import Flask, redirect, url_for
import praw
import logging

from review_api.models import Base, engine, Session
from review_api.config import config

app = Flask(__name__)
app.config.from_object(config)

Base.metadata.create_all(engine)

logging.basicConfig(
    filename=config.LOG_FILE,
    format=config.LOG_FORMAT,
    datefmt=config.LOG_DATEFORMAT,
    style=config.LOG_FORMAT_STYLE,
    level=config.LOG_LEVEL
)

@app.route('/')
def home():
    return redirect(url_for('portal.home'))

@app.before_request
def before_request():
    Session() # create a Session local to the request

@app.after_request
def after_request(response):
    Session.remove()
    return response

from review_api.views import auth
app.register_blueprint(auth.blueprint, url_prefix="/auth")

from review_api.views import api
app.register_blueprint(api.blueprint, url_prefix="/api")

from review_api.views import portal
app.register_blueprint(portal.blueprint, url_prefix="/portal")

from review_api.views.errors import *
