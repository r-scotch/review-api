import logging, os

BASE_URL = 'http://localhost:5000'
CONFIG_FILE = __name__

# SqlAlchemy config
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join('data', 'api.db')

# Flask config
DEBUG = True
TRAP_HTTP_EXCEPTIONS = True
SECRET_KEY = 'something-that-really-should-be-automagically-generated'
PORT = 5000
HOST='0.0.0.0'

# Logging config
LOG_TO_FILE = True
LOG_FILE = os.path.join('logs', 'api.log')
LOG_LEVEL = logging.DEBUG

LOG_FORMAT = '{asctime} | {levelname:^8} | {message}'
LOG_DATEFORMAT = '%Y-%m-%d %H:%M:%S'
LOG_FORMAT_STYLE = '{'

# Security config
TOKEN_EXP = 60 * 10
JWT_ALG = 'HS256'

# Api config
RESULT_LIMIT = 500

# Reddit config
REDDIT_CLIENT_ID = ''
REDDIT_CLIENT_SECRET = ''
REDDIT_ACCESS_URL = 'https://www.reddit.com/api/v1/access_token'
REDDIT_AUTH_URL = 'https://www.reddit.com/api/v1/authorize'
REDDIT_USER_AGENT = ''
REDDIT_REDIRECT_PATH = '/portal/reddit'
