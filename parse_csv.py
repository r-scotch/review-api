import praw
import pdb
import hashlib
import logging
import sys
from datetime import date, datetime

from sqlalchemy.exc import IntegrityError, InvalidRequestError
from archiveparser import ArchiveParser
from review_api.models import Reviewer, Meta, Bottle, Review, Session

logger = logging.getLogger('ParseJob')
logger.setLevel(logging.ERROR)
fh = logging.FileHandler('logs/parser.log')
fh.setLevel(logging.ERROR)
logger.addHandler(fh)

def get_bottle(name, session):
    return session.query(Bottle).filter(Bottle.name == name.lower()).first()

def insert_bottle(name, region, session):
    bottle = Bottle(name=name.lower(), region=region.lower())
    session.add(bottle)
    session.commit()
    return bottle

def get_reviewer(name, session):
    return session.query(Reviewer).filter(Reviewer.name == name.lower()).first()

def insert_reviewer(name, session):
    reviewer = Reviewer(name=name.lower(), display_name=name)
    session.add(reviewer)
    session.commit()
    return reviewer

def hash_review(row):
    id_hash = hashlib.md5()
    if type(row['reviewer']) is str:
        id_hash.update(row['reviewer'].lower().encode('utf-8'))
    else:
        id_hash.update(row['reviewer'].name.lower().encode('utf-8'))
    id_hash.update(row['link'].lower().encode('utf-8'))
    if type(row['bottle']) is str:
        id_hash.update(row['bottle'].lower().encode('utf-8'))
    else:
        id_hash.update(row['bottle'].name.lower().encode('utf-8'))
    return id_hash

def get_review(rid, session):
    return session.query(Review).filter(Review.id == rid).first()

def row_to_review(row):
    id_hash = hash_review(row)
    return Review(
        id       = id_hash.hexdigest(),
        bottle   = row['bottle'],
        reviewer = row['reviewer'],
        link     = row['link'],
        rating   = row['rating'],
        price    = row['price'],
        date     = row['date'],
        title    = row['title'],
        subreddit= row['subreddit'].lower()
    )

def insert_review(row, session):
    bottle = get_bottle(row['bottle'], session)
    if bottle is None:
        bottle = insert_bottle(row['bottle'], row['region'], session)

    reviewer = get_reviewer(row['reviewer'], session)
    if reviewer is None:
        reviewer = insert_reviewer(row['reviewer'], session)

    row['bottle'] = bottle
    row['reviewer'] = reviewer

    session.add(row_to_review(row))

def get_missing_data(review, reddit, override=False):
    # Only get missing data, if vital data is missing
    # Requesting from the Reddit API slows down the parsing process
    review['title'] = None
    if override or not review['date'] or not review['subreddit']:
        try:
            submission = parser.get_submission(review, reddit)
            if not submission:
                return review
            review['date'] = date.fromtimestamp(submission.created_utc)
            review['title'] = submission.title
            review['subreddit'] = submission.subreddit.display_name
        except Exception as e:
            logger.exception(
                'Unexpected exception while retrieving submission: ' + review['link']
            )
    return review

if __name__ == '__main__':
    user_agent = 'Whisky Archive Parser by /u/FlockOnFire'
    reddit = praw.Reddit(user_agent=user_agent)
    parser = ArchiveParser('data/archive.csv')

    session = Session()
    archive = parser.get_rows()
    override = len(sys.argv) > 1
    print(len(sys.argv))
    for index, review in enumerate(archive):
        if index % 500 == 0:
            print(index)
        review_id = hash_review(review).hexdigest()
        db_review = get_review(review_id, session)
        if db_review:
            continue
        review = get_missing_data(review, reddit, override)
        if not review['date'] or not review['subreddit']:
            logger.error('Cannot insert review #{} ({}, {}): date = {} \t subreddit = {}'
                .format(index, review['reviewer'], review['bottle'], review['date'], review['subreddit'])
            )
            continue
        try:
            insert_review(review, session)
        except IntegrityError as e:
            session.rollback()
        except Exception as e:
            logger.exception('Unexpected exception while inserting review.', e)
            session.rollback()
    session.commit()
    time = datetime.utcnow().timestamp()
    Meta.update('database_updated', int(time))
