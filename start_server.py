from review_api import app, config
import logging

if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.info('Configuration: ' + config.CONFIG_FILE)
    if config.DEBUG:
        print('Warning: DEBUG is enabled.')
    app.run(host=config.HOST, port=config.PORT, debug=True)

