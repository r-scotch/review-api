# /r/Scotch - ReviewApi

The **ReviewApi** is a REST API for the Whisky Review Archive ([/r/scotch](https://reddit.com/r/scotch), [/r/bourbon](https://reddit.com/r/bourbon), [/r/worldwhisky](https://reddit.com/r/worldwhisky)]).
All the data is parsed from the archive using the [ArchiveParser](https://gitlab.com/r-scotch/archiveparser/). For now, it is not yet possible to create a precise enough classifier to retrieve reviews including bottle name and score from Reddit submission/comments.

## Installation

1. Install the ArchiveParser: `pip install https://gitlab.com/r-scotch/archiveparser/repository/archive.tar.gz`
2. Install the required dependencies: `pip install -R api.requirements`
3. Copy and fill in the configuration file: `review_api/config/dummy.py`
4. Link the right configuration in `review_api/config/__init.py__`
5. Download the archive to `data/archive.csv` and parse it: `python parse_csv.py`
6. Start the server: `python start_server.py`